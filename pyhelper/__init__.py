# -*- coding: utf-8 -*-
__author__='Delio Castillo <jangeador@gmail.com>'
import os
import time
from sys import platform as _platform


def validate_path(path):
    # if _platform == "linux" or _platform == "linux2":
    #     # linux
    # elif _platform == "darwin":
    #     # OS X
    # elif _platform == "win32":
        # Windows...
    pass


def time_stamp(file_safe=False):
    """
    
    returns a formatted current time/date
    :rtype : string
    """
    if file_safe:
        return str(time.strftime('%Y%m%d-%H%M%S'))
    else:
        return str(time.strftime("%a %d %b %Y %I:%M:%S %p"))


def get_or_create_folder(path):
    """

    :param path: string
    :return: string
    """
    if not os.path.exists(path):
        os.makedirs(path)
    return os.path.join(path)
