import unittest
import pyhelper


class PyHelperTests(unittest.TestCase):

    def test_test_working(self):
        self.assertEqual(1, 1)


def main():
    unittest.main()


if __name__ == '__main__':
    main()