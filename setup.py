from distutils.core import setup

setup(
    name='djc-pyhelper',
    version='0.1.1',
    author='Delio Castillo',
    author_email='jangeador@gmail.com',
    packages=['pyhelper'],
    scripts=[],
    url='https://bitbucket.org/jangeador/djc-pyhelper/',
    license='LICENSE.txt',
    description='A module with some helper functions for python.',
    long_description=open('README.txt').read(),
)